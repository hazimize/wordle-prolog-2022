kata(1,logic).
kata(2,prolg).
kata(3,amber).
kata(4,swish).
kata(5,bluff).
kata(6,clash).
kata(7,apple).
kata(8,compu).
kata(9,vegan).
kata(10,never).
kata(11,fight).
kata(12,thick).
kata(13,anger).
kata(14,equal).
kata(15,quail).

position([H1,H2,H3,H4,H5],[A1,A2,A3,A4,A5],Result):-
   % Cek posisi antara tebakan dengan kata misteri 
   (H1=A1 -> append([],["T"], X1);  append([],["F"], X1)),
   (H2=A2 -> append(X1, ["T"], X2);  append(X1,["F"], X2)),
   (H3=A3 -> append(X2,["T"], X3);  append(X2,["F"], X3)),
   (H4=A4 -> append(X3,["T"], X4);  append(X3,["F"], X4)),
   (H5=A5 -> append(X4,["T"], X);  append(X4,["F"], X)),

   % Mengambil karakter yang salah ditebak
   my_pop(X, [A1, A2, A3, A4, A5], [], PopResult),

   % Verifikasi ulang tebakan
   % jika terdapat karakter yang bernilai F namun hanya karena kesalahan posisi
   % maka akan diganti dari F menjadi X
   changeToX(PopResult, [H1,H2,H3,H4,H5], X, [], Result).



changeToX(_, [], [],Result,Result). 
changeToX(PopResult, [_|TGuess], [HBool|TBool],DummyRes, Result):-
    HBool = "T",!, append(DummyRes, ["T"], NewDummyRes), changeToX(PopResult,TGuess,TBool,NewDummyRes,Result),!.
changeToX(PopResult, [HGuess|TGuess], [HBool|TBool],DummyRes, Result):-
    HBool = "F", !, member(HGuess, PopResult) -> indexOf(PopResult, HGuess, Idx),
                                                   away(PopResult, Idx, NewPopResult),
                                                   append(DummyRes, ["X"], NewDummyRes),
                                                   changeToX(NewPopResult,TGuess,TBool,NewDummyRes,Result);
                                                    append(DummyRes, ["F"], NewDummyRes),
                                                    changeToX(PopResult,TGuess,TBool,NewDummyRes,Result).  
                                  

replace([_|T], 1, X, [X|T]).
replace([H|T], I, X, [H|R]):- I > 0, NI is I-1, replace(T, NI, X, R), !.
replace(L, _, _, L).

indexOf([Element|_], Element, 1):- !.
indexOf([_|Tail], Element, Index):-
  indexOf(Tail, Element, Index1),
  !,
  Index is Index1+1.

away([_|H],1,H):-!.
away([G|H],N,[G|L]):- N > 1, Nn is N - 1,!,away(H,Nn,L).

add_tail([],X,[X]).
add_tail([H|T],X,[H|L]):- add_tail(T,X,L).

my_pop([], [], Y, Y).
my_pop([H|T], [HW|TW], EmptyList, Res) :-
   (H =\= "T" ->  add_tail(EmptyList, HW, X),
       my_pop(T, TW, X, Res);
   my_pop(T, TW, EmptyList, Res)).

concat_program([X],X).
concat_program([L,L1|Ls], Str) :-
	string_concat(L,L1, Str0),
    concat_program([Str0|Ls], Str).

capitalize([], []).
capitalize([H1|T1], [H2|T2]):-
  code_type(H2, to_upper(H1)), capitalize(T1, T2).

atoms_to_list_of_string([], []).
atoms_to_list_of_string([H1|T1], [H2|T2]) :-
  atom_string(H1, H2), atoms_to_list_of_string(T1, T2).

random_question(LetterList) :- 
    findall(W, kata(_, W), List),
    random_select(X, List, _),
    atom_codes(X, ListOfAtom),
    capitalize(ListOfAtom, UpperListOfAtom),
    atom_codes(AlphabetAtoms, UpperListOfAtom),
    atom_chars(AlphabetAtoms, ListAlphabetAtoms),
    atoms_to_list_of_string(ListAlphabetAtoms, LetterList).

:- initialization(main).
    
main:-
    random_question(RandomWord),
    repeat,
    format(user_output,'Please enter words (or quit): ', []),
    read_line_to_codes(user_input, Codes1),
    (
        atom_codes(quit, Codes1) ->
            !
        ;
        capitalize(Codes1, Codes2) ->
            atom_codes(Words, Codes2),
            atom_chars(Words, LetterListAtom),
            atoms_to_list_of_string(LetterListAtom, LetterList),

            format(current_output, '\nWords: ~a\n', [Words]),
            
            position(LetterList, RandomWord, X),  
            concat_program(X,Str),
            write(Str), nl,
            fail
        ;
            fail
    ).